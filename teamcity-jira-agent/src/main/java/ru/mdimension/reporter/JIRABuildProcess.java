package ru.mdimension.reporter;

import jetbrains.buildServer.RunBuildException;
import jetbrains.buildServer.agent.*;
import org.jetbrains.annotations.NotNull;
import ru.mdimension.reporter.jira.Reporter;
import ru.mdimension.reporter.jira.Ticket;
import ru.mdimension.reporter.teamcity.TeamCityApiParser;

import java.util.List;


public class JIRABuildProcess implements BuildProcess {
    private final AgentRunningBuild myBuild;
    private final BuildRunnerContext myContext;
    private BuildProgressLogger logger;
    private RunnerParamsProvider props;


    public JIRABuildProcess(@NotNull AgentRunningBuild build, @NotNull BuildRunnerContext context) {
        myBuild = build;
        myContext = context;
    }

    @Override
    public void start() throws RunBuildException {
        props = new RunnerParamsProvider();
        logger = myBuild.getBuildLogger();
        logger.message("Start jira plugin");

        myContext.getRunnerParameters().entrySet().stream()
                .forEach(entry -> props.setProperty(entry.getKey(), entry.getValue()));
        props.setProperty("git.commit.url", myContext.getConfigParameters().get("git.commit.url"));
        props.setProperty("git.user.url",myContext.getConfigParameters().get("git.user.url"));
        props.setProperty("teamcity.serverUrl",myContext.getConfigParameters().get("teamcity.serverUrl"));
        props.setProperty("build.type.id", myContext.getBuild().getBuildTypeId());
        List<Ticket> tickets = new TeamCityApiParser(logger, props).getTickets();
        new Reporter(logger, props).report(tickets);
    }

    @Override
    public boolean isInterrupted() {
        return false;
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void interrupt() {
    }

    @NotNull
    @Override
    public BuildFinishedStatus waitFor() throws RunBuildException {
        return BuildFinishedStatus.FINISHED_SUCCESS;
    }
}
