
package ru.mdimension.reporter.teamcity.rest;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "id",
    "version",
    "username",
    "date",
    "href",
    "webUrl",
    "comment",
    "files",
    "vcsRootInstance"
})
public class Change {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("version")
    private String version;
    @JsonProperty("username")
    private String username;
    @JsonProperty("date")
    private String date;
    @JsonProperty("href")
    private String href;
    @JsonProperty("webUrl")
    private String webUrl;
    @JsonProperty("comment")
    private String comment;
    @JsonProperty("files")
    private Files files;
    @JsonProperty("vcsRootInstance")
    private VcsRootInstance vcsRootInstance;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The version
     */
    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    /**
     * 
     * @param version
     *     The version
     */
    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * 
     * @return
     *     The username
     */
    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    /**
     * 
     * @param username
     *     The username
     */
    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 
     * @return
     *     The date
     */
    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The href
     */
    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    /**
     * 
     * @param href
     *     The href
     */
    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * 
     * @return
     *     The webUrl
     */
    @JsonProperty("webUrl")
    public String getWebUrl() {
        return webUrl;
    }

    /**
     * 
     * @param webUrl
     *     The webUrl
     */
    @JsonProperty("webUrl")
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    /**
     * 
     * @return
     *     The comment
     */
    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    /**
     * 
     * @param comment
     *     The comment
     */
    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * 
     * @return
     *     The files
     */
    @JsonProperty("files")
    public Files getFiles() {
        return files;
    }

    /**
     * 
     * @param files
     *     The files
     */
    @JsonProperty("files")
    public void setFiles(Files files) {
        this.files = files;
    }

    /**
     * 
     * @return
     *     The vcsRootInstance
     */
    @JsonProperty("vcsRootInstance")
    public VcsRootInstance getVcsRootInstance() {
        return vcsRootInstance;
    }

    /**
     * 
     * @param vcsRootInstance
     *     The vcsRootInstance
     */
    @JsonProperty("vcsRootInstance")
    public void setVcsRootInstance(VcsRootInstance vcsRootInstance) {
        this.vcsRootInstance = vcsRootInstance;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
