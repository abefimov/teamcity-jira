
package ru.mdimension.reporter.teamcity.rest.build;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "version",
    "vcs-root-instance"
})
public class Revision {

    @JsonProperty("version")
    private String version;
    @JsonProperty("vcs-root-instance")
    private VcsRootInstance vcsRootInstance;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The version
     */
    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    /**
     * 
     * @param version
     *     The version
     */
    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * 
     * @return
     *     The vcsRootInstance
     */
    @JsonProperty("vcs-root-instance")
    public VcsRootInstance getVcsRootInstance() {
        return vcsRootInstance;
    }

    /**
     * 
     * @param vcsRootInstance
     *     The vcs-root-instance
     */
    @JsonProperty("vcs-root-instance")
    public void setVcsRootInstance(VcsRootInstance vcsRootInstance) {
        this.vcsRootInstance = vcsRootInstance;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
