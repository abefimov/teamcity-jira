
package ru.mdimension.reporter.teamcity.rest.build;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "id",
    "buildTypeId",
    "number",
    "status",
    "state",
    "href",
    "webUrl",
    "statusText",
    "buildType",
    "queuedDate",
    "startDate",
    "finishDate",
    "triggered",
    "lastChanges",
    "changes",
    "revisions",
    "agent",
    "artifacts",
    "relatedIssues",
    "statistics"
})
public class BuildFullInfo {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("buildTypeId")
    private String buildTypeId;
    @JsonProperty("number")
    private String number;
    @JsonProperty("status")
    private String status;
    @JsonProperty("state")
    private String state;
    @JsonProperty("href")
    private String href;
    @JsonProperty("webUrl")
    private String webUrl;
    @JsonProperty("statusText")
    private String statusText;
    @JsonProperty("buildType")
    private BuildType buildType;
    @JsonProperty("queuedDate")
    private String queuedDate;
    @JsonProperty("startDate")
    private String startDate;
    @JsonProperty("finishDate")
    private String finishDate;
    @JsonProperty("triggered")
    private Triggered triggered;
    @JsonProperty("lastChanges")
    private LastChanges lastChanges;
    @JsonProperty("changes")
    private Changes changes;
    @JsonProperty("revisions")
    private Revisions revisions;
    @JsonProperty("agent")
    private Agent agent;
    @JsonProperty("artifacts")
    private Artifacts artifacts;
    @JsonProperty("relatedIssues")
    private RelatedIssues relatedIssues;
    @JsonProperty("statistics")
    private Statistics statistics;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The buildTypeId
     */
    @JsonProperty("buildTypeId")
    public String getBuildTypeId() {
        return buildTypeId;
    }

    /**
     * 
     * @param buildTypeId
     *     The buildTypeId
     */
    @JsonProperty("buildTypeId")
    public void setBuildTypeId(String buildTypeId) {
        this.buildTypeId = buildTypeId;
    }

    /**
     * 
     * @return
     *     The number
     */
    @JsonProperty("number")
    public String getNumber() {
        return number;
    }

    /**
     * 
     * @param number
     *     The number
     */
    @JsonProperty("number")
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * 
     * @return
     *     The status
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The state
     */
    @JsonProperty("state")
    public String getState() {
        return state;
    }

    /**
     * 
     * @param state
     *     The state
     */
    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    /**
     * 
     * @return
     *     The href
     */
    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    /**
     * 
     * @param href
     *     The href
     */
    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * 
     * @return
     *     The webUrl
     */
    @JsonProperty("webUrl")
    public String getWebUrl() {
        return webUrl;
    }

    /**
     * 
     * @param webUrl
     *     The webUrl
     */
    @JsonProperty("webUrl")
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    /**
     * 
     * @return
     *     The statusText
     */
    @JsonProperty("statusText")
    public String getStatusText() {
        return statusText;
    }

    /**
     * 
     * @param statusText
     *     The statusText
     */
    @JsonProperty("statusText")
    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    /**
     * 
     * @return
     *     The buildType
     */
    @JsonProperty("buildType")
    public BuildType getBuildType() {
        return buildType;
    }

    /**
     * 
     * @param buildType
     *     The buildType
     */
    @JsonProperty("buildType")
    public void setBuildType(BuildType buildType) {
        this.buildType = buildType;
    }

    /**
     * 
     * @return
     *     The queuedDate
     */
    @JsonProperty("queuedDate")
    public String getQueuedDate() {
        return queuedDate;
    }

    /**
     * 
     * @param queuedDate
     *     The queuedDate
     */
    @JsonProperty("queuedDate")
    public void setQueuedDate(String queuedDate) {
        this.queuedDate = queuedDate;
    }

    /**
     * 
     * @return
     *     The startDate
     */
    @JsonProperty("startDate")
    public String getStartDate() {
        return startDate;
    }

    /**
     * 
     * @param startDate
     *     The startDate
     */
    @JsonProperty("startDate")
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * 
     * @return
     *     The finishDate
     */
    @JsonProperty("finishDate")
    public String getFinishDate() {
        return finishDate;
    }

    /**
     * 
     * @param finishDate
     *     The finishDate
     */
    @JsonProperty("finishDate")
    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    /**
     * 
     * @return
     *     The triggered
     */
    @JsonProperty("triggered")
    public Triggered getTriggered() {
        return triggered;
    }

    /**
     * 
     * @param triggered
     *     The triggered
     */
    @JsonProperty("triggered")
    public void setTriggered(Triggered triggered) {
        this.triggered = triggered;
    }

    /**
     * 
     * @return
     *     The lastChanges
     */
    @JsonProperty("lastChanges")
    public LastChanges getLastChanges() {
        return lastChanges;
    }

    /**
     * 
     * @param lastChanges
     *     The lastChanges
     */
    @JsonProperty("lastChanges")
    public void setLastChanges(LastChanges lastChanges) {
        this.lastChanges = lastChanges;
    }

    /**
     * 
     * @return
     *     The changes
     */
    @JsonProperty("changes")
    public Changes getChanges() {
        return changes;
    }

    /**
     * 
     * @param changes
     *     The changes
     */
    @JsonProperty("changes")
    public void setChanges(Changes changes) {
        this.changes = changes;
    }

    /**
     * 
     * @return
     *     The revisions
     */
    @JsonProperty("revisions")
    public Revisions getRevisions() {
        return revisions;
    }

    /**
     * 
     * @param revisions
     *     The revisions
     */
    @JsonProperty("revisions")
    public void setRevisions(Revisions revisions) {
        this.revisions = revisions;
    }

    /**
     * 
     * @return
     *     The agent
     */
    @JsonProperty("agent")
    public Agent getAgent() {
        return agent;
    }

    /**
     * 
     * @param agent
     *     The agent
     */
    @JsonProperty("agent")
    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    /**
     * 
     * @return
     *     The artifacts
     */
    @JsonProperty("artifacts")
    public Artifacts getArtifacts() {
        return artifacts;
    }

    /**
     * 
     * @param artifacts
     *     The artifacts
     */
    @JsonProperty("artifacts")
    public void setArtifacts(Artifacts artifacts) {
        this.artifacts = artifacts;
    }

    /**
     * 
     * @return
     *     The relatedIssues
     */
    @JsonProperty("relatedIssues")
    public RelatedIssues getRelatedIssues() {
        return relatedIssues;
    }

    /**
     * 
     * @param relatedIssues
     *     The relatedIssues
     */
    @JsonProperty("relatedIssues")
    public void setRelatedIssues(RelatedIssues relatedIssues) {
        this.relatedIssues = relatedIssues;
    }

    /**
     * 
     * @return
     *     The statistics
     */
    @JsonProperty("statistics")
    public Statistics getStatistics() {
        return statistics;
    }

    /**
     * 
     * @param statistics
     *     The statistics
     */
    @JsonProperty("statistics")
    public void setStatistics(Statistics statistics) {
        this.statistics = statistics;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
