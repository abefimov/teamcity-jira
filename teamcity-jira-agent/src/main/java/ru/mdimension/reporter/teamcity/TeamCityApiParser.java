package ru.mdimension.reporter.teamcity;


import com.fasterxml.jackson.databind.ObjectMapper;
import jetbrains.buildServer.agent.BuildProgressLogger;
import ru.mdimension.reporter.RunnerParamsProvider;
import ru.mdimension.reporter.jira.Ticket;
import ru.mdimension.reporter.teamcity.rest.Builds;
import ru.mdimension.reporter.teamcity.rest.Change;
import ru.mdimension.reporter.teamcity.rest.RelatedIssues;
import ru.mdimension.reporter.teamcity.rest.build.BuildFullInfo;
import sun.misc.BASE64Encoder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TeamCityApiParser {
    private List<Ticket> tickets;
    private BuildProgressLogger buildProgressLogger;
    private RunnerParamsProvider props;
    private String userCreds;

    public static final String RELATED_ISSUES_ = "/httpAuth/app/rest/builds/id:";
    public static final String BUILDS_ = "/httpAuth/app/rest/builds?locator=branch:default:any,running:true,buildType:";

    /**
     * @param logger
     */
    public TeamCityApiParser(BuildProgressLogger logger, RunnerParamsProvider props) {
        this.props = props;
        this.buildProgressLogger = logger;
        String serverUrl = props.getTCServerUrl();
        userCreds = props.getTCUser() + ":" + props.getTCPassword();
        try {
            tickets = new ArrayList<>();
            Builds buildsData = getBuilds(serverUrl + BUILDS_ + props.getBuildTypeId());
            RelatedIssues relatedIssues = getRelatedIssues(serverUrl + RELATED_ISSUES_ +
                    buildsData.getBuild().get(0).getId() + "/relatedIssues");
            relatedIssues.getIssueUsage().stream()
                    .forEach(issueUsage -> tickets.add(new Ticket(issueUsage.getIssue(),
                            issueUsage.getChanges().getChange().stream()
                                    .map(change -> getChange(serverUrl + change.getHref()))
                                    .collect(Collectors.toList()))));
            props.setBuild(getBuildFullInfo(serverUrl + buildsData.getBuild().get(0).getHref()));
        } catch (Exception e) {
            buildProgressLogger.exception(e);
        }
    }

    private <T> T makeRequest(String teamCityUrl, Class<T> clazz) {
        try {
            URL url = new URL(teamCityUrl);
            String encoding = new BASE64Encoder().encode(userCreds.getBytes());
            URLConnection uc = url.openConnection();
            uc.setRequestProperty("Authorization", "Basic " + encoding);
            uc.setRequestProperty("Accept", "application/json");
            uc.connect();

            BufferedReader streamReader = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            StringBuilder responseStrBuilder = new StringBuilder();
            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(responseStrBuilder.toString().getBytes(), clazz);
        } catch (IOException e) {
            buildProgressLogger.exception(e);
        }
        return null;
    }

    private Builds getBuilds(String teamCityUrl) {
        return makeRequest(teamCityUrl, Builds.class);
    }

    private Change getChange(String teamCityUrl) {
        return makeRequest(teamCityUrl, Change.class);
    }

    private BuildFullInfo getBuildFullInfo(String teamCityUrl) {
        return makeRequest(teamCityUrl, BuildFullInfo.class);
    }

    private RelatedIssues getRelatedIssues(String teamCityUrl) {
        return makeRequest(teamCityUrl, RelatedIssues.class);
    }

    public List<Ticket> getTickets() {
        return tickets;
    }
}
