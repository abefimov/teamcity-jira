
package ru.mdimension.reporter.teamcity.rest;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "id",
    "buildTypeId",
    "number",
    "status",
    "state",
    "running",
    "percentageComplete",
    "href",
    "webUrl"
})
public class Build {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("buildTypeId")
    private String buildTypeId;
    @JsonProperty("number")
    private String number;
    @JsonProperty("status")
    private String status;
    @JsonProperty("state")
    private String state;
    @JsonProperty("running")
    private Boolean running;
    @JsonProperty("percentageComplete")
    private Integer percentageComplete;
    @JsonProperty("href")
    private String href;
    @JsonProperty("webUrl")
    private String webUrl;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The buildTypeId
     */
    @JsonProperty("buildTypeId")
    public String getBuildTypeId() {
        return buildTypeId;
    }

    /**
     * 
     * @param buildTypeId
     *     The buildTypeId
     */
    @JsonProperty("buildTypeId")
    public void setBuildTypeId(String buildTypeId) {
        this.buildTypeId = buildTypeId;
    }

    /**
     * 
     * @return
     *     The number
     */
    @JsonProperty("number")
    public String getNumber() {
        return number;
    }

    /**
     * 
     * @param number
     *     The number
     */
    @JsonProperty("number")
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * 
     * @return
     *     The status
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The state
     */
    @JsonProperty("state")
    public String getState() {
        return state;
    }

    /**
     * 
     * @param state
     *     The state
     */
    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    /**
     * 
     * @return
     *     The running
     */
    @JsonProperty("running")
    public Boolean getRunning() {
        return running;
    }

    /**
     * 
     * @param running
     *     The running
     */
    @JsonProperty("running")
    public void setRunning(Boolean running) {
        this.running = running;
    }

    /**
     * 
     * @return
     *     The percentageComplete
     */
    @JsonProperty("percentageComplete")
    public Integer getPercentageComplete() {
        return percentageComplete;
    }

    /**
     * 
     * @param percentageComplete
     *     The percentageComplete
     */
    @JsonProperty("percentageComplete")
    public void setPercentageComplete(Integer percentageComplete) {
        this.percentageComplete = percentageComplete;
    }

    /**
     * 
     * @return
     *     The href
     */
    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    /**
     * 
     * @param href
     *     The href
     */
    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * 
     * @return
     *     The webUrl
     */
    @JsonProperty("webUrl")
    public String getWebUrl() {
        return webUrl;
    }

    /**
     * 
     * @param webUrl
     *     The webUrl
     */
    @JsonProperty("webUrl")
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
