package ru.mdimension.reporter.jira;


import ru.mdimension.reporter.teamcity.rest.Change;
import ru.mdimension.reporter.teamcity.rest.Issue;

import java.util.List;

/**
 * Class that contains related issue data
 *
 * Created by efimov
 * on 23.03.16.
 */
public class Ticket {

    /** Jira issue */
    private Issue issue;

    /** List of git changes for issue */
    private List<Change> changes;

    public Ticket(Issue issue, List<Change> changes) {
        this.issue = issue;
        this.changes = changes;
    }

    public Issue getIssue() {
        return issue;
    }

    public List<Change> getChanges() {
        return changes;
    }
}
