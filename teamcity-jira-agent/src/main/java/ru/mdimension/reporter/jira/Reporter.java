package ru.mdimension.reporter.jira;

import com.google.common.base.Splitter;
import jetbrains.buildServer.agent.BuildProgressLogger;
import net.rcarz.jiraclient.*;
import org.jetbrains.annotations.NotNull;
import ru.mdimension.reporter.RunnerParamsProvider;
import ru.mdimension.reporter.teamcity.rest.Change;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Reporter {
    private BuildProgressLogger myLogger;
    private RunnerParamsProvider props;

    public Reporter(BuildProgressLogger myLogger, RunnerParamsProvider props) {
        this.myLogger = myLogger;
        this.props = props;
    }

    public void report(List<Ticket> tickets) {
        Map<String, List<Change>> ticketsRes = new HashMap<>();
        tickets.stream().collect(Collectors.groupingBy(ticket -> ticket.getIssue().getId()))
                .forEach((k, v) -> ticketsRes.put(k, v.stream()
                        .flatMap(ticket -> ticket.getChanges().stream()).collect(Collectors.toList())));

        JiraClient jira = getJiraClient();
        ticketsRes.entrySet().forEach(ticket -> processTicket(jira, ticket.getKey(), ticket.getValue()));
    }

    @NotNull
    private JiraClient getJiraClient() {
        BasicCredentials creds = new BasicCredentials(props.getJiraUser(), props.getJiraPassword());
        return new JiraClient(props.getJiraServerUrl(), creds);
    }

    private void processTicket(JiraClient jira, String ticket, List<Change> changes) {
        Issue issue;
        try {
            issue = jira.getIssue(ticket);
        } catch (JiraException e) {
            myLogger.exception(e);
            return;
        }
        try {
            myLogger.message("ISSUE: " + ticket
                    + "\nTitle: " + issue.getSummary()
                    + "\nDescription: " + issue.getDescription());
            issue.addComment(createComment(changes));
            if (props.progressIssueIsEnable()) {
                transitionIssue(props.getJiraWorkFlow(), issue);
            }
        } catch (JiraException e) {
            myLogger.exception(e);
        }
    }

    private String createComment(List<Change> changes) throws JiraException {

        String comment;
        if (props.useJiraCommentTemplate()) {
            comment = props.getJiraCommentTemplate();
        } else {
            comment = "Fixed by: " + changes.stream().map(Change::getUsername).distinct()
                    .map(user -> getJiraUrl(user, props.getGitUserUrl()))
                    .collect(Collectors.joining(", ")) + "\n" +
                    "Commits: " + changes.stream().map(Change::getVersion)
                    .map(commit -> getJiraUrl(commit, props.getCommitUrl()))
                    .collect(Collectors.joining(", ")) + "\n" +
                    "With comments:\n" + changes.stream().map(Change::getComment)
                    .collect(Collectors.joining()) + "\n" +
                    "Build: " + props.getBuild().getBuildType().getProjectName() + " :: " +
                    props.getBuild().getBuildType().getName() +
                    ", build number #" + props.getBuild().getNumber();
        }
        myLogger.message("comment = " + comment);
        return comment;
    }

    @NotNull
    private String getJiraUrl(String commit, String url) {
        return "[" + commit + "|" + url + commit + "]";
    }

    private void transitionIssue(String transitionFormat, Issue issue) {
        Map<String, String> transitionMap = Splitter.on(",").trimResults().omitEmptyStrings()
                .withKeyValueSeparator(":").split(transitionFormat);
        Status existingStatus = issue.getStatus();
        if (!transitionMap.keySet().contains(existingStatus.getName())) {
            myLogger.warning("Issue " + issue.getKey() + " cannot change status");
            return;
        }
        move(issue, existingStatus, transitionMap.get(existingStatus.getName()));
    }

    private void move(Issue issue, Status existingStatus, String newStatus) {
        if (newStatus == null || newStatus.equals("")) {
            myLogger.error("Failed transition Jira issue [ " + issue.getKey() +
                    " ] from the status [ " + existingStatus + " ], cannot resolve new status ");
            return;
        }
        try {
            myLogger.message(String.format("Changing state for issue %s from %s to %s",
                    issue.getId(), existingStatus, newStatus));
            issue.transition().execute(newStatus);
        } catch (JiraException e) {
            myLogger.error("Failed transition Jira issue [ " + issue.getId() +
                    " ] from the status [ " + existingStatus + " ]" +
                    "to the new status [ " + newStatus + " ] ");
            myLogger.exception(e);
        }
    }
}
