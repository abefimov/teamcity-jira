package ru.mdimension.reporter;


import ru.mdimension.reporter.teamcity.rest.build.BuildFullInfo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class RunnerParamsProvider {

    private BuildFullInfo build;

    private Properties props = new Properties();

    public RunnerParamsProvider() {
        try {
            FileInputStream fis = new FileInputStream("params.properties");
            props.load(fis);
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public Set<Map.Entry<Object, Object>> test() {
       return props.entrySet();
    }

    public void setProperty(String key, String value) {
        try {
            props.setProperty(key, value);
            props.store(new FileOutputStream("params.properties"), "set parameters");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getJiraServerUrl() {
        return props.getProperty("jiraServerUrl");
    }

    public BuildFullInfo getBuild() {
        return build;
    }

    public void setBuild(BuildFullInfo build) {
        this.build = build;
    }

    public String getJiraUser() {
        return props.getProperty("jiraUser");
    }

    public String getJiraPassword() {
        return props.getProperty("jiraPassword");
    }

    public String getBuildTypeId() {
        return props.getProperty("build.type.id");
    }

    public String getJiraWorkFlow() {
        return props.getProperty("jiraWorkflow");
    }

    public String getTCServerUrl() {
        return props.getProperty("teamcity.serverUrl");
    }

    public String getTCUser() {
        return props.getProperty("tcUser");
    }

    public String getTCPassword() {
        return props.getProperty("tcPassword");
    }

    public Boolean progressIssueIsEnable() {
        return Boolean.valueOf(props.getProperty("enableIssueProgressing"));
    }

    public Boolean useJiraCommentTemplate() {
        return Boolean.valueOf(props.getProperty("useJiraCommentTemplate"));
    }

    public String getGitUserUrl() {
        return props.getProperty("git.user.url");
    }

    public String getCommitUrl() {
        return props.getProperty("git.commit.url");
    }

    public String getJiraCommentTemplate() {
        return props.getProperty("jiraCommentTemplate");
    }

}
