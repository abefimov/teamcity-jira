<%@ include file="/include.jsp" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%--<jsp:useBean id="propertiesBean" scope="request" type="jetbrains.buildServer.controllers.BasePropertiesBean"/>--%>

<c:set var="title" value="JIRA Reporter" scope="request"/>


<l:settingsGroup title="JIRA configuration">
    <tr>
        <td>
            <span>Server URL:</span>
            <br>
            <props:textProperty name="jiraServerUrl" className="longField"/>
            <br>
            <span>User:</span>
            <br>
            <props:textProperty name="jiraUser"/>
            <br>
            <span>Password:</span>
            <br>
            <props:passwordProperty name="jiraPassword"/>
            <br>
            <span>Set your comment:</span>
            <props:checkboxProperty name="useJiraCommentTemplate"/>
            <br>
            <div id="jira.comment.template">
                <props:multilineProperty name="jiraCommentTemplate" rows="7" cols="58"
                                         linkTitle="Enter your comment template"/>
            </div>
            <br>
            <span>Enable issue progressing:</span>
            <props:checkboxProperty name="enableIssueProgressing"/>
            <br>
            <div id="jira.workflow">
                <props:multilineProperty name="jiraWorkflow" rows="5" cols="58"
                                         linkTitle="Enter your JIRA workflow for issue progressing"/>
            </div>
            <br>
        </td>
    </tr>
</l:settingsGroup>
<l:settingsGroup title="TeamCity Configuration">
    <tr>
        <td>
            <span>User:</span>
            <br>
            <props:textProperty name="tcUser"/>
            <br>
            <span>Password:</span>
            <br>
            <props:passwordProperty name="tcPassword"/>
        </td>
    </tr>
</l:settingsGroup>

<script type="text/javascript">
    var checkBox = jQuery('#enableIssueProgressing');
    checkBox.change(function () {
        if (jQuery(this).prop("checked")) {
            BS.Util.show('jira.workflow');
        }
        else {
            BS.Util.hide('jira.workflow');
        }
        BS.VisibilityHandlers.updateVisibility('mainContent');
    });
    if (checkBox.prop("checked")) {
        BS.Util.show('jira.workflow');
    }
    else {
        BS.Util.hide('jira.workflow');
    }
    var tplComment = jQuery('#useJiraCommentTemplate');
    tplComment.change(function () {
        if (jQuery(this).prop("checked")) {
            BS.Util.show('jira.comment.template');
        }
        else {
            BS.Util.hide('jira.comment.template');
        }
        BS.VisibilityHandlers.updateVisibility('mainContent');
    });
    if (tplComment.prop("checked")) {
        BS.Util.show('jira.comment.template');
    }
    else {
        BS.Util.hide('jira.comment.template');
    }
    BS.MultilineProperties.updateVisible();
    BS.VisibilityHandlers.updateVisibility('mainContent');
</script>
